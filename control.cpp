/* See LICENSE file for copyright and license details. */

#include "control.h"

Control::Control():
pwm(pwm_pin)
{
	sensors = new Sensors();
	pid = new Pid();
	ref = init_angle;

	pwm.period_ms(motor_period);
	pwm = motor_min/motor_period;

}

Control::~Control()
{
}

void
Control::getsample()
{
	int angle, ee;
	float ne, ny, y;

	/* Error */
	angle = sensors->getangle();
	ee = angle-ref;
	ne = ee/(float)90;

	#ifdef DEBUG
	printf(	"[control] error: %03d\n"
		"[control] normalize error: %3.2f\n",
		ee, ne);
	#endif

	/* PID */
	ny = pid->update(ne);
	y = ( motor_max*(1.0+ny) + motor_min*(1.0-ny)) /(2*motor_period);

	if (y<(motor_min/motor_period))
		y = motor_min/motor_period;
	if ((motor_max/motor_period)<y)
		y = motor_max/motor_period;

	pwm = y;

	#ifdef DEBUG
	printf(	"[control] normalize control: %3.2f\n"
		"[control] control : %4.3f\n",
		ny, y);
	#endif

}
