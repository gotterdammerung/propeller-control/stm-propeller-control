/* See LICENSE file for copyright and license details. */

#ifndef SENSORS__HH
#define SENSORS__HH

#include "mbed.h"

#include "MPU6050.h"

#include "config.h"

#define PI 3.14159265

class Sensors
{
public:
	Sensors();
	virtual ~Sensors();

	int getangle();
private:
	MPU6050 *mpu;
};

#endif
