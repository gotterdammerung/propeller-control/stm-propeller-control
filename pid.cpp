/* See LICENSE file for copyright and license details. */

#include "pid.h"

Pid::Pid()
{
	kp = init_kp;
	kd = init_kd;
	ki = init_ki;
}

Pid::~Pid()
{
}

float
Pid::update(float ee)
{
	float dd, ii, y;

	/* Check error */
	if (ee<-1.0)
		ee = -1.0;
	if (1.0<ee)
		ee = 1.0;

	/* Differential */
	dd = ee-ed;
	ed = ee;

	/* Integral */
	ii += ee*tsample;
	if (ii<-1.0)
		ii = -1.0;
	if (1.0<ii)
		ii = 1.0;

	/* Control */
	y = kp*ee + kd*ed + ki*ii;

	#ifdef DEBUG
	printf("[pid] response y=%3.2f\n", y);
	#endif

	return y;
}
