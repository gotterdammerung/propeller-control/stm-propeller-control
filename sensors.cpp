/* See LICENSE file for copyright and license details. */

#include "sensors.h"

Sensors::Sensors()
{
	mpu = new MPU6050(mpu_sda, mpu_scl);
	mpu->start();
}

Sensors::~Sensors()
{
}

int
Sensors::getangle()
{
	float gx, gy, gz;
	float ax, ay, az;
	int angle;

	mpu->read(&gx, &gy, &gz, &ax, &ay, &az);

	#ifdef DEBUG
	printf(	"[sensors] mpu: get data\n"
		"[sensors] mpu: gx=%04.2f\n"
		"[sensors] mpu: gy=%04.2f\n"
		"[sensors] mpu: gz=%04.2f\n"
		"[sensors] mpu: ax=%04.2f\n"
		"[sensors] mpu: ay=%04.2f\n"
		"[sensors] mpu: az=%04.2f\n",
		gx, gy, gz, ax, ay, az);

	#endif

	/* Only I and IV quadrant */
	if ((az<0.01)&&(0.0<ax))
		angle = 90;
	else if ((az<0.01)&&(ax<0.0))
		angle = -90;
	else
		angle = atan(ax/az)*180/PI;

	#ifdef DEBUG
	printf("[sensors] angle=%02d\n", angle);
	#endif

	return angle;
}
