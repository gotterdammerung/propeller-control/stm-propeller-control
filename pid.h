/* See LICENSE file for copyright and license details. */

#ifndef PID__HH
#define PID__HH

#include "mbed.h"

#include "config.h"

class Pid
{
public:
	Pid();
	virtual ~Pid();

	float update(float ee);
private:
	float kp, kd, ki;
	float ed;
};

#endif
