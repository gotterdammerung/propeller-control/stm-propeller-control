/* See LICENSE file for copyright and license details. */

#ifndef CONFIG_HH
#define CONFIG_HH

#define DEBUG

/* Serial port */
static const unsigned int serialbaudrate = 115200;

/* Sample time*/
#define TSAM	100ms
static const float tsample = 100e-3;

/* MPU6050 Pin */
static const PinName mpu_scl = D15;
static const PinName mpu_sda = D14;

/* PWM Pin */
static const PinName pwm_pin = D9;
/* Motor controller */
static const float motor_period = 20; /* period in ms */
static const float motor_min = 1; /* min pulse in ms */
static const float motor_max = 2; /* max pulse in ms */


/* PID */
static const float init_kp = 1;
static const float init_kd = 0.0;
static const float init_ki = 0.0;

/* Angle */
static const int init_angle = 0.0;

#endif
