/* See LICENSE file for copyright and license details. */

#ifndef CONTROL__HH
#define CONTROL__HH

#include "mbed.h"

#include "pid.h"
#include "sensors.h"

#include "config.h"

class Control
{
public:
	Control();
	virtual ~Control();

	void getsample();
private:
	Sensors *sensors;
	Pid *pid;

	PwmOut pwm;

	int ref;
};

#endif
