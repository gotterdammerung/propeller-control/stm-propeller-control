/* See LICENSE file for copyright and license details. */

#include "mbed.h"

#include "control.h"

#include "config.h"

/* Functions declarations */
void getsample(void);
void postevents(void);

/* Declarations */
DigitalOut	led(LED1);

Control *control;

/* Protocols */
BufferedSerial	pc(USBTX, USBRX);

/* Events */
EventQueue queue;
Event<void(void)> eventread(&queue, getsample);

/* Functions implementations */
void
getsample()
{
	#ifdef DEBUG
	printf("[main] Event\n");
	#endif
	control->getsample();
	return;
}

void
postevents()
{
	eventread.post();
}

int
main()
{
	/* Protocols */
	pc.set_baud(serialbaudrate);

	control = new Control();

	/* Check FPU */
	#if (__FPU_PRESENT == 1) && (__FPU_USED == 1)
	#ifdef DEBUG
	printf("[main] Use FPU function (compiler enables FPU)\n");
	#endif
	#endif

	/* Config events */
	Thread eventthread;

	eventread.delay(TSAM);
	eventread.period(TSAM);

	eventthread.start(callback(postevents));

	/* Initialization */
	led = 1;
	queue.dispatch_forever();

	/* End */
	led = 0;
	return 0;
}
